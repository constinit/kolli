#include "service_registry.hpp"

#include <map>
#include <date/date.h>

namespace kolli
{

  class DummyRegister
  {
    public:
      std::optional<ParcelInfo> get(std::string const & trackingID)
      {
        auto date = std::chrono::system_clock::now();
        auto & p = registry[trackingID];
        p.id = trackingID;
        p.events.push_back(
            {
              .description = date::format("Dummy %F %X", date),
              .date = date,
              .location = {
                .name = "DummyPlace",
              },
            }
            );
        return p;
      }

    private:
      std::map<std::string, ParcelInfo> registry;
  };

  static std::optional<ParcelInfo> fetch(std::string const & trackingID)
  {
    static DummyRegister reg;
    return reg.get(trackingID);
  }

  [[gnu::used]] ServiceRegistration dummy_service("dummy", fetch);
}
