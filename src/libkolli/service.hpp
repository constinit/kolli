#pragma once

#include <chrono>
#include <optional>
#include <string>
#include <vector>

namespace kolli
{

  struct Location
  {
    std::string name;
    auto operator<=>(Location const &) const = default;
  };

  struct Event
  {
    std::string description;
    std::chrono::system_clock::time_point date;
    Location location;
    auto operator<=>(Event const &) const = default;
  };

  struct ParcelInfo
  {
    std::string id;
    std::vector<Event> events;
    std::optional<std::chrono::system_clock::time_point> estimatedArrival;
    auto operator<=>(ParcelInfo const &) const = default;
    ParcelInfo operator-(ParcelInfo const & other) const;
  };

  std::string fetchUrl(std::string const & url, std::vector<std::string> const & headers = {});
  std::string post(std::string const & url, std::string const& postData, std::vector<std::string> const & headers = {});
  [[nodiscard]] std::string printParcelInfo(ParcelInfo parcelInfo);
  [[nodiscard]] std::string printParcelEvents(ParcelInfo parcelInfo);
  std::optional<ParcelInfo> fetch(std::string const & service, std::string const & trackingID);
  std::chrono::system_clock::time_point parseDate(std::string const & str);

}
