#include "service_registry.hpp"
#include <nlohmann/json.hpp>

namespace kolli
{
  static std::string buildUrl(std::string const & id)
  {
    return "https://tracking.dpd.de/rest/plc/en_US/" + id;
  }

  static std::optional<Event> getEventInfo(nlohmann::json const & json)
  {
    return Event{
      .description = json["scanDescription"]["content"][0],
      .date = parseDate(json["integrationDate"]),
      .location = {
        .name = json["scanData"]["location"],
      },
    };
  }

  static std::optional<ParcelInfo> getParcelInfo(nlohmann::json const & json)
  {
    ParcelInfo p;
    for (auto const & s : json["parcellifecycleResponse"]["parcelLifeCycleData"]["scanInfo"]["scan"])
      if (auto e = getEventInfo(s))
        p.events.push_back(*e);
    return p;
  }

  static std::optional<ParcelInfo> fetch(std::string const & trackingID)
  {
    try
    {
      return getParcelInfo(nlohmann::json::parse(fetchUrl(buildUrl(trackingID))));
    }
    catch (...)
    {
      return {};
    }
  }

  [[gnu::used]] ServiceRegistration dpd_service("dpd", fetch);
}
