#include "service_registry.hpp"
#include <chrono>
#include <nlohmann/json.hpp>
#include <ranges>

namespace kolli
{
  auto const trackingUrl = "https://azure-cn.orangeconnex.com/oc/capricorn-website/website/v1/tracking/traces";

  static std::string buildRequest(std::string const& trackingID)
  {
    return "{\"trackingNumbers\":[\"" + trackingID + "\"]}";
  }

//{
//  "hasBusinessException": false,
//  "result": {
//    "notExistsTrackingNumbers": [],
//    "waybills": [
//      {
//        "consigneeCityCode": "Umeå",
//        "consigneeCityName": "Umeå",
//        "consigneeCountryCode": "SE",
//        "consigneeCountryName": "Sweden",
//        "consigneeZipCode": "90728",
//        "consignmentCityCode": "440300",
//        "consignmentCityName": "SHENZHEN",
//        "consignmentCountryCode": "CN",
//        "consignmentCountryName": "China",
//        "consignmentZipCode": "518000",
//        "isEventCode": "MANIFEST",
//        "lastStatus": "Shipment Data Received ",
//        "lastTime": "2021-03-26 09:04:02",
//        "lastTimeZone": "8",
//        "lastTimestamp": 1616720642506,
//        "traces": [
//          {
//            "eventDesc": "Shipment Data Received ",
//            "oprTime": "2021-03-26 09:04:02",
//            "oprTimeZone": "8",
//            "oprTimestamp": 1616720642506
//          }
//        ],
//        "trackingNumber": "XXXXXXXXXXXXXXXXXXXXXXXX"
//      }
//    ]
//  },
//  "success": true
//}

  static Event getEventInfo(nlohmann::json const & json)
  {
    auto const ms = std::chrono::milliseconds{json["oprTimestamp"].get<int64_t>()};
    auto const location = json.value("oprCity", "");

    return Event{
        .description = json["eventDesc"],
        .date = std::chrono::system_clock::time_point(ms),
        .location = {.name = location },
    };
  }

  static std::optional<ParcelInfo> getParcelInfo(nlohmann::json const & json)
  {
    ParcelInfo p;
    for (auto const & wb : json["result"]["waybills"])
      for (auto const &t : wb["traces"])
        p.events.push_back(getEventInfo(t));

    std::ranges::sort(p.events, [](auto const& a, auto const& b) { return a.date < b.date; });

    return p;
  }

  static std::optional<ParcelInfo> fetch(std::string const & trackingID)
  {
    try
    {
      auto const response = post(trackingUrl, buildRequest(trackingID), { "Content-Type: application/json;charset=utf-8" } );
      return getParcelInfo(nlohmann::json::parse(response));
    }
    catch (...)
    {
      return {};
    }
  }

  [[gnu::used]] ServiceRegistration orange_service("orange", fetch);
}
