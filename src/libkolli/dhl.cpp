#include "service_registry.hpp"
#include <nlohmann/json.hpp>
#include <iostream>
#include <thread>
#include <chrono>

namespace kolli
{
  static std::string buildUrl(std::string const & id)
  {
    return "https://api-eu.dhl.com/track/shipments?language=en&trackingNumber=" + id;
//    return "https://www.dhl.com/shipmentTracking?countryCode=g0&languageCode=en&AWB=" + id;
  }

  static Event getEventInfo(nlohmann::json const & json)
  {
    return Event{
      .description = json["description"],
      .date = parseDate(json["timestamp"]),
        .location = {
          .name = json.contains("location") ? json["location"]["address"]["addressLocality"] : "",
        },
    };
  }

  static std::optional<ParcelInfo> getParcelInfo(nlohmann::json const & json)
  {
    if (json.count("shipments") > 0)
    {
      ParcelInfo p;
      for (auto const & s : json["shipments"])
      {
        p.id = s["id"];
        for (auto const & e : s["events"])
          p.events.push_back(getEventInfo(e));
      }
      std::reverse(p.events.begin(), p.events.end());
      return p;
    }
    return {};
  }

  static std::optional<ParcelInfo> fetch(std::string const & trackingID)
  {
    try
    {
      std::this_thread::sleep_for(std::chrono::seconds(10));
      return getParcelInfo(nlohmann::json::parse(fetchUrl(buildUrl(trackingID), { "DHL-API-Key: demo-key" })));
    }
    catch (...)
    {
      return {};
    }
  }

  [[gnu::used]] ServiceRegistration dhl_service("dhl", fetch);
}
