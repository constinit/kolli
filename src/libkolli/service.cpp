#include "service.hpp"

#include "service_registry.hpp"
#include "curl_session.hpp"

#include <date/date.h>
#include <sstream>

namespace kolli
{
  ParcelInfo ParcelInfo::operator-(ParcelInfo const & other) const
  {
    ParcelInfo p;
    for (auto const & e : events)
      if (std::find(other.events.begin(), other.events.end(), e) == other.events.end())
        p.events.push_back(e);
    return p;
  }

  std::string fetchUrl(std::string const & url, std::vector<std::string> const & headers)
  {
    return CurlSession().get(url, headers);
  }

  std::string post(std::string const & url, std::string const& postData, std::vector<std::string> const & headers)
  {
    return CurlSession().post(url, postData, headers);
  }

  std::string printParcelInfo(ParcelInfo parcelInfo)
  {
    std::stringstream response;
    response << parcelInfo.id << "\n";
    if (parcelInfo.estimatedArrival)
      response << " Estimated arrival: " << date::format("%F %X", *parcelInfo.estimatedArrival) << "\n";
    return response.str();
  }

  std::string printParcelEvents(ParcelInfo parcelInfo)
  {
    std::stringstream response;
    for (auto const & e : parcelInfo.events)
      response << date::format("%F %X", e.date) << " [ " << e.location.name << " ] " << e.description << "\n";
    return response.str();
  }

  std::optional<ParcelInfo> fetch(std::string const & service, std::string const & trackingID)
  {
    return ServiceRegistry::fetch(service, trackingID);
  }

  std::chrono::system_clock::time_point parseDate(std::string const & str)
  {
    std::istringstream in(str);
    date::sys_seconds tp;
    in >> date::parse("%FT%TZ", tp);
    if (in.fail())
    {
      in.clear();
      in.str(str);
      in >> date::parse("%FT%T", tp);
    }
    return tp;
  }

}
