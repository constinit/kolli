#pragma once

#include <map>
#include <optional>
#include <vector>
#include "service.hpp"

namespace kolli
{

  class ServiceRegistry
  {
    public:
      using FetchType = std::optional<ParcelInfo>;
      using FetchFunction = FetchType(*)(std::string const&);
      using ServiceRegistryType = std::map<std::string, FetchFunction>;

      static bool registerService(std::string service, FetchFunction func);
      static FetchType fetch(std::string const & service, std::string const & trackingID);
      static std::vector<std::string> list();

    private:
      static ServiceRegistryType s_registry;
  };

  class ServiceRegistration
  {
    public:
      ServiceRegistration(std::string service, ServiceRegistry::FetchFunction func)
      {
        ServiceRegistry::registerService(service, func);
      }
  };

}
